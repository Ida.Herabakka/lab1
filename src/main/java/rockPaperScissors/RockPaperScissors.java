package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in); //read from user input
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        //loop
        while (true){
            System.out.println("Let's play round " + roundCounter);
            String humanChoise = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            String computerChoise = getRandom(rpsChoices);
            System.out.print("Human chose " + humanChoise+", computer chose "+computerChoise + ". ");
            
            if(is_winner(humanChoise, computerChoise)){
                System.out.println(" Human wins!");
                humanScore ++;
            }
            else if(is_winner(computerChoise, humanChoise)){
                System.out.println(" Computer wins!");
                computerScore ++;
            }
            else{
                System.out.println("It's a tie!");
            }
            roundCounter ++;
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String playAgain = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (playAgain.equals("n")){
                break;
            }
            else{
                continue;
            }

        }
        System.out.println("Bye bye :)");
        
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String getRandom(List<String> rpsChoices){
        Random random = new Random();
        return rpsChoices.get(random.nextInt(rpsChoices.size()));
        
    } 
    public boolean is_winner(String choise1, String choise2){
        if(choise1.equals("paper")){
            return choise2.equals("rock");
        }
        else if(choise1.equals("scissors")){
            return choise2.equals("paper");
        }
        else{
            return choise2.equals("scissors");
        }
    }

}
